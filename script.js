Array.prototype.indexOf = function(obj, start) {
     for (var i = (start || 0), j = this.length; i < j; i++) {
         if (this[i] === obj) { return i; }
     }
     return -1;
} //IE support


var basic_input = [{"id":1,"start":30,"end":90},
		{"id":2,"start":540,"end":600},
		{"id":3,"start":560,"end":620},
		{"id":4,"start":610,"end":670}];

var complex_input2 = [
	{"id":1,"start":0,"end":120},
	{"id":2,"start":60,"end":200},
	{"id":3,"start":90,"end":380},
	{"id":4,"start":180,"end":300},
	{"id":5,"start":240,"end":360},
	{"id":6,"start":320,"end":400},
	{"id":7,"start":390,"end":500},
	{"id":8,"start":510,"end":600}];

/*
Replace json_input with the json array of your choice.
Included are two examples. The basic one and a more complex one.
*/
json_input = complex_input2;

//take output and print it out
$(document).ready(function()
{
	//time
	var start_time = 9;
	var end_time = 21;
	var offset = -10; //offset in pixels so text is at the middle of the point, not just below it.
	for(var i=0; i<=(end_time-start_time)*2; i++)
	{
		var hour = start_time+i/2;
		var am_pm = "<span class='am_pm_text'>AM</span>";

		if(Math.floor(hour) > 12) //pm
		{
			hour -= 12;
			am_pm = "<span class='am_pm_text'>PM</span>";
		}

		if(i%2==0) //Every hour
		{
			$('#time_box').append("<div class='hour_text'>" + hour  + ":00 " + am_pm + "</div>");
		}
		else //every half-hour
		{
			$('#time_box').append("<div class='half_hour_text'>" + Math.floor((hour))  + ":30 </div>");
		}
	}

	//lay out and calibrate the events for the day	
	var events = layOutDay(json_input);

	//add events to page
	for(var i=0;i<events.length;i++)
	{
		$('#main').append("<div id='event_" + events[i]['id'] + "'><div class='blue_box'></div><div class='event_details'><span class='event_header'>Sample Item</span> <br /><span class='event_location'> Sample Location</span></div></div>");
		$("#event_"+events[i]['id']).css({
			'position': 'absolute',
			'margin-left' : degree,
			'left': events[i]['left']+10, //+ 10 for padding
			'top': events[i]['top'],
			'height': events[i]['end']-events[i]['start'], 
			'width': events[i]['width'],
			'overflow': 'hidden',
			'background-color': '#ffffff',
			'border': '1px solid #d6d6d6'
		});
	}
});

/*Method:
Degree = how many events in the timeslot. (ie, 2 overlapping events means both events will have a degree of 2)
Position = where in the degree it should be. (1 = most-left)

The function works by aligning events underneath each other at each degree/level.
When all non-clashing events are aligned at that level, it moves to the next level.
When an event is aligned, it is checked against the already aligned events to see if it collides and subsequently adjusts its degree.

Not all events directly collide, so a final recalibration is done to update the degrees so that its width is the same as the events it indirectly and directly collide with.
*/
function layOutDay(events)
{
	var iterate_list = events //list to iterate through
	var aligned_list = new Array(); //already aligned
	var unaligned_list = new Array(); //still to align

	iterate_list.sort(function(a,b) { return a.start - b.start }); //sort based on start time

	degree = 1; //all events start out as if they are the only one in that timeslot.

	//iterate through events until all of them have been aligned
	while(iterate_list.length > 0)
	{	
		//go through sorted events based on start time
		for(var i=0;i<iterate_list.length;i++)
		{
			for(var j=i;j<iterate_list.length;j++) //iterate onwards
			{
				//if already in the unaligned list, don't check.
				if(unaligned_list.indexOf(iterate_list[j])!=-1)
				{
					break;
				}

				/*
				First event on that degree/level will always have a left-most position because it has been sorted on start time.(i==j)
				find the first event that has a start time great than initial end time
				*/
				if(iterate_list[j]['start'] > iterate_list[i]['end'] || i==j) 
				{
					if(aligned_list.indexOf(iterate_list[j])==-1)
					{
						//add event to aligned list.	
						iterate_list[j]['degree'] = degree;
						iterate_list[j]['position'] = degree;

						//iterate through aligned list to determine whether to increase the degree of the already aligned events.
						for(var a = 0;a<aligned_list.length;a++)
						{
							if(iterate_list[j]['start'] <= aligned_list[a]['end'] && iterate_list[j]['end'] >= aligned_list[a]['start'])
							{
								aligned_list[a]['degree'] = degree;
							}
						}

						aligned_list.push(iterate_list[j]);
						
						//now continue checking onwards from this event.
						i=j-1
						break;
					}
				}
				else
				{
					//add event to unaligned list.
					unaligned_list.push(iterate_list[j]);
				}
			}
		}

		iterate_list = unaligned_list;
		unaligned_list = new Array();
		degree++; //now start iterating on the level.
	}

	/*
	Final recalibration.
	Some degrees are not properly calibrated because not all events overlap directly.
	Look back at the previous element and if it collides, change degree to match.
	*/
	aligned_list.sort(function(a,b) { return a.start - b.start});
	for(var i=1;i<aligned_list.length;i++)
	{
		if(aligned_list[i]['start'] <= aligned_list[i-1]['end'])
		{
			if(aligned_list[i]['degree'] < aligned_list[i-1]['degree'])
			{
				aligned_list[i]['degree'] = aligned_list[i-1]['degree'];
			}
			else if(aligned_list[i]['degree'] > aligned_list[i-1]['degree'])
			{
				aligned_list[i-1]['degree'] = aligned_list[i]['degree'];
			}
		}
	}

	var c_width = 600; //calendar width
	//iterate through events and add width/left top positions.
	for(var i in aligned_list)
	{
		events[i]['top'] = aligned_list[i]['start']; //top pixel is the start time
		events[i]['left'] = c_width/aligned_list[i]['degree']*(aligned_list[i]['position']-1); //Left most pixel
		events[i]['width'] = c_width/aligned_list[i]['degree']-1;
	}

	return events;
}
